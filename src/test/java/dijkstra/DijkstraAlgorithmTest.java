package dijkstra;

import dijkstra.DijkstraAlgorithm.Edge;
import dijkstra.DijkstraAlgorithm.Graph;
import dijkstra.DijkstraAlgorithm.Vertex;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * 测试算法
 *
 * @author Kevin
 */
public class DijkstraAlgorithmTest {

    private static List<Vertex> nodes = new ArrayList<>();
    private static List<Edge> edges = new ArrayList<>();

    static {
        for (int i = 0; i < 11; i++) {
            nodes.add(new Vertex("Node_" + i, "Node_" + i));
        }

        addLane("Edge_0", 0, 1, 85);
        addLane("Edge_1", 0, 2, 217);
        addLane("Edge_2", 0, 4, 173);
        addLane("Edge_3", 2, 6, 186);
        addLane("Edge_4", 2, 7, 103);
        addLane("Edge_5", 3, 7, 183);
        addLane("Edge_6", 5, 8, 250);
        addLane("Edge_7", 8, 9, 84);
        addLane("Edge_8", 7, 9, 167);
        addLane("Edge_9", 4, 9, 502);
        addLane("Edge_10", 9, 10, 40);
        addLane("Edge_11", 1, 10, 600);
    }

    @Test
    public void testFindPath() {
        // Lets check from location Loc_1 to Loc_10
        Graph graph = new Graph(nodes, edges);
        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
        dijkstra.execute(nodes.get(0));
        LinkedList<Vertex> path = dijkstra.getPath(nodes.get(10));

        System.out.println("The path is :");
        for (Vertex vertex : path) {
            System.out.println(vertex);
        }
    }

    /**
     * 添加连线
     *
     * @param laneId      编号
     * @param sourceLocNo 起点ID
     * @param desLocNo    终点ID
     * @param duration    权值
     */
    private static void addLane(String laneId, int sourceLocNo, int desLocNo, int duration) {
        Edge lane = new Edge(laneId, nodes.get(sourceLocNo), nodes.get(desLocNo), duration);
        edges.add(lane);
    }
}
