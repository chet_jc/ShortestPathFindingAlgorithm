package tk.winshu.shortestpath.operate;

import tk.winshu.shortestpath.model.Node;

import java.util.List;

/**
 * 抽象的操作
 *
 * @author winshu
 * @since 2019/10/29
 */
public interface IOperate {

    /**
     * 执行操作
     *
     * @return 操作结果
     */
    boolean execute();

    /**
     * 回退操作
     */
    void revert();

    /**
     * 设置节点集
     *
     * @param nodes 节点集
     */
    default void init(List<Node> nodes) {
    }
}
