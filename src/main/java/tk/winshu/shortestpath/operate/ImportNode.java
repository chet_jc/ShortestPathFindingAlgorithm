package tk.winshu.shortestpath.operate;

import tk.winshu.shortestpath.model.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * 导入操作
 *
 * @author winshu
 * @since 2019/10/29
 */
@OperateType(name = "导入数据")
public class ImportNode extends AbstractOperate {

    private List<Node> importNodes;
    private List<Node> historyNodes;

    public ImportNode(List<Node> importNodes) {
        this.importNodes = importNodes;
    }

    @Override
    public void init(List<Node> nodes) {
        super.init(nodes);
        this.historyNodes = new ArrayList<>(nodes);
    }

    @Override
    public boolean execute() {
        nodes.clear();
        nodes.addAll(importNodes);
        return true;
    }

    @Override
    public void revert() {
        nodes.clear();
        nodes.addAll(historyNodes);
    }
}
