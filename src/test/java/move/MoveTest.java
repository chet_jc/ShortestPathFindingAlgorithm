package move;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * 测试斜线移动
 */
public class MoveTest extends JFrame {

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                MoveTest frame = new MoveTest();
                frame.setVisible(true);
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        });
    }

    private MoveTest() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        CanvasPanel canvasPanel = new CanvasPanel();
        contentPane.add(canvasPanel, BorderLayout.CENTER);

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.EAST);
        panel.setLayout(new BorderLayout(0, 0));

        JButton btnRun = new JButton("Run");
        btnRun.addActionListener((e) -> canvasPanel.run());
        panel.add(btnRun);
    }

}
