package dijkstra2;

import org.junit.Test;
import tk.winshu.shortestpath.algorithm.Dijkstra;
import tk.winshu.shortestpath.model.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * @author winshu
 * @since 2019/5/28
 */
public class DijkstraTest {

    private static List<Node> nodes = new ArrayList<>();

    static {
        // index 从 1 开始，避开 0 的干扰
        nodes.add(node(1, 0, 0));
        nodes.add(node(2, 10, 0));
        nodes.add(node(3, 0, 10));
        nodes.add(node(4, 10, 10));

        line(1, 2);
        line(2, 4);
        line(4, 3);
        line(3, 1);
//        line(1, 4);
        line(2, 3);
    }

    @Test
    public void testDijkstra() {
        Node start = node(4);
        Node end = node(1);

        Dijkstra dijkstra = new Dijkstra(false);
        List<Node> path = dijkstra.find(start, end);
        System.out.println(path);
    }

    private static Node node(int index, int x, int y) {
        return new Node(index, x, y);
    }

    private static Node node(int index) {
        return nodes.get(index - 1);
    }

    private static boolean line(int startIndex, int endIndex) {
        Node start = nodes.get(startIndex - 1);
        Node end = nodes.get(endIndex - 1);

        if (start == null || end == null) {
            return false;
        }
        return start.relationTo(end);
    }
}
