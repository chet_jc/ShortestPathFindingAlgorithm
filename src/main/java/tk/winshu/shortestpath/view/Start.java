package tk.winshu.shortestpath.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;

/**
 * @author winshu
 * @date 2015年2月10日
 */
public class Start {

    private static final Logger log = LoggerFactory.getLogger(Start.class);

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                JFrame.setDefaultLookAndFeelDecorated(true);
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
                MainFrame frame = new MainFrame();
                frame.setSize(800, 600);

                Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
                frame.setLocation((screen.width - frame.getWidth()) / 2, (screen.height - frame.getHeight()) / 2);

                frame.setVisible(true);
            } catch (Exception e) {
                log.error("运行异常", e);
            }
        });
    }

}
