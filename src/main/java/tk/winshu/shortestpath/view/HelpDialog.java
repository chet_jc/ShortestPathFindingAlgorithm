package tk.winshu.shortestpath.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tk.winshu.shortestpath.model.NodeStatus;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 * @author winshu
 * @date 2015年2月10日
 */
public class HelpDialog extends JDialog {

    private static final Logger log = LoggerFactory.getLogger(HelpDialog.class);

    public static void main(String[] args) {
        HelpDialog dialog = new HelpDialog();
        dialog.setVisible(true);
    }

    public HelpDialog() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("帮助");
        setIconImage(NodeStatus.DEFAULT.getImage());
        setModal(true);

        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((screen.width - getWidth()) / 2, (screen.height - getHeight()) / 2);
        setBounds(100, 100, 600, 500);

        createContent();
    }

    private void createContent() {
        final JScrollPane scrollPane = new JScrollPane();
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(scrollPane, BorderLayout.CENTER);

        try {
            JEditorPane editorPane = new JEditorPane(HelpDialog.class.getResource("/doc/help.html"));
            editorPane.setEditable(false);
            editorPane.setContentType("text/html;charset=UTF-8");
            scrollPane.setViewportView(editorPane);
        } catch (IOException e1) {
            log.error(e1.getMessage(), e1);
        }

        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);

        JButton closeButton = new JButton("关闭");
        closeButton.addActionListener(e -> HelpDialog.this.dispose());
        buttonPane.add(closeButton);
        getRootPane().setDefaultButton(closeButton);

        JViewport viewPort = scrollPane.getViewport();
        viewPort.setViewPosition(new Point(0, 0));
    }

}
