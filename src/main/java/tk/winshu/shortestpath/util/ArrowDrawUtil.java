package tk.winshu.shortestpath.util;

import java.awt.*;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * 绘制箭头算法
 *
 * @author winshu
 * @date 2015年2月5日
 */
public class ArrowDrawUtil {

    /**
     * 箭头长度
     */
    private static final double ARROW_LENGTH = 16;
    /**
     * 底边的长度
     */
    private static final double ARROW_HALF_WIDTH = 4;

    /**
     * 绘制带箭头的线
     */
    public static void draw(Graphics2D g2, Point2D start, Point2D end) {
        draw(g2, start.getX(), start.getY(), end.getX(), end.getY());
    }

    /**
     * 绘制带箭头的线
     */
    private static void draw(Graphics2D g2, double startX, double startY, double endX, double endY) {
        // 箭头角度
        double arrowAngle = Math.atan(ARROW_HALF_WIDTH / ARROW_LENGTH);
        // 箭头的长度
        double arrowLength = Math.sqrt(ARROW_HALF_WIDTH * ARROW_HALF_WIDTH + ARROW_LENGTH * ARROW_LENGTH);
        double[] rotateVec1 = rotateVec(endX - startX, endY - startY, arrowAngle, arrowLength);
        double[] rotateVec2 = rotateVec(endX - startX, endY - startY, -arrowAngle, arrowLength);
        // (x3,y3)是第一端点
        double x3 = endX - rotateVec1[0];
        double y3 = endY - rotateVec1[1];
        // (x4,y4)是第二端点
        double x4 = endX - rotateVec2[0];
        double y4 = endY - rotateVec2[1];

        g2.draw(new Line2D.Double(startX, startY, endX, endY));

        GeneralPath triangle = new GeneralPath();
        triangle.moveTo(endX, endY);
        triangle.lineTo(x3, y3);
        triangle.lineTo(x4, y4);
        triangle.closePath();

        g2.fill(triangle);
    }

    /**
     * 计算旋转角度
     *
     * @param ang 角度
     */
    private static double[] rotateVec(double px, double py, double ang, double newLen) {
        double[] mathstr = new double[2];
        // 矢量旋转函数，参数含义分别是x分量、y分量、旋转角、是否改变长度、新长度
        double vx = px * Math.cos(ang) - py * Math.sin(ang);
        double vy = px * Math.sin(ang) + py * Math.cos(ang);
        double d = Math.sqrt(vx * vx + vy * vy);
        vx = vx / d * newLen;
        vy = vy / d * newLen;
        mathstr[0] = vx;
        mathstr[1] = vy;

        return mathstr;
    }

}
