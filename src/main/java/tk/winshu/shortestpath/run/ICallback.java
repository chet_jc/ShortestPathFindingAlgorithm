package tk.winshu.shortestpath.run;

/**
 * @author winshu
 * @since 2020/11/08.
 */
@FunctionalInterface
public interface ICallback {

    void onFinished();
}
