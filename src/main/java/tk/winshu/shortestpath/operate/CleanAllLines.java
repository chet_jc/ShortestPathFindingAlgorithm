package tk.winshu.shortestpath.operate;

import java.util.ArrayList;
import java.util.List;

/**
 * 清空连线
 *
 * @author winshu
 * @since 2019/10/29
 */
@OperateType(name = "清空连线")
public class CleanAllLines extends AbstractOperate {

    private List<Line> lines;

    public CleanAllLines() {
        this.lines = new ArrayList<>();
    }

    @Override
    public boolean execute() {
        // 存下所有连线
        nodes.forEach(start -> start.getEndNodes().forEach(end -> lines.add(new Line(start, end))));
        // 移除连线关系
        lines.forEach(Line::release);
        return true;
    }

    @Override
    public void revert() {
        // 重建连线关系
        lines.forEach(Line::relation);
    }


}
