package tk.winshu.shortestpath.operate;

import tk.winshu.shortestpath.model.Node;

/**
 * @author winshu
 * @since 2019/10/29
 */
class Line {

    private Node start;
    private Node end;

    Line(Node start, Node end) {
        this.start = start;
        this.end = end;
    }

    boolean relation() {
        return start.relationTo(end);
    }

    void release() {
        start.releaseRelation(end);
    }
}
