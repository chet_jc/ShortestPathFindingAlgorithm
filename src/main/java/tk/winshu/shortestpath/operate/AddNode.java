package tk.winshu.shortestpath.operate;

import tk.winshu.shortestpath.model.Node;

import java.awt.geom.Point2D;
import java.util.List;

/**
 * 添加节点
 *
 * @author winshu
 * @since 2019/10/28
 */
@OperateType(name = "添加节点")
public class AddNode extends AbstractOperate {

    private final Point2D point;
    private Node node;

    public AddNode(Point2D point) {
        this.point = point;
    }

    @Override
    public void init(List<Node> nodes) {
        super.init(nodes);
        // 生成新节点
        int nextNodeIndex = nodes.isEmpty() ? 1 : getNode(nodes.size() - 1).getIndex() + 1;
        this.node = new Node(nextNodeIndex, point);
    }

    @Override
    public boolean execute() {
        return addNode(node);
    }

    @Override
    public void revert() {
        removeNode(node);
    }

    /**
     * 生成下一个节点的标识ID
     */
    private int nextNodeIndex() {
        return nodes.isEmpty() ? 1 : getNode(nodes.size() - 1).getIndex() + 1;
    }
}
