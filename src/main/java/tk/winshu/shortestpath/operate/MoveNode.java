package tk.winshu.shortestpath.operate;

import tk.winshu.shortestpath.model.Node;
import tk.winshu.shortestpath.model.Runner;

import java.awt.geom.Point2D;

/**
 * 移动节点
 *
 * @author winshu
 * @since 2019/10/29
 */
@OperateType(name = "移动节点")
public class MoveNode implements IOperate {

    private Node node;
    private Runner runner;
    private Point2D point;
    private Point2D historyPoint;

    public MoveNode(Point2D point, Node node, Runner runner) {
        this.point = point;
        this.node = node;
        this.runner = runner;
        this.historyPoint = node.getLocation();
    }

    @Override
    public boolean execute() {
        node.setLocation(point);
        // runner 在当前节点上，一并移动
        if (runner != null && runner.getIndex() == node.getIndex()) {
            runner.setLocation(point);
        }
        return true;
    }

    @Override
    public void revert() {
        node.setLocation(historyPoint);
        // runner 在当前节点上，一并移动
        if (runner != null && runner.getIndex() == node.getIndex()) {
            runner.setLocation(historyPoint);
        }
    }
}
