package tk.winshu.shortestpath.operate;

import tk.winshu.shortestpath.model.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * 移除节点所在连线
 *
 * @author winshu
 * @since 2019/10/29
 */
@OperateType(name = "清空节点连线")
public class CleanLines implements IOperate {

    private Node node;
    private List<Line> lines;

    public CleanLines(Node node) {
        this.node = node;
        this.lines = new ArrayList<>();
    }

    @Override
    public boolean execute() {
        // 存储节点关联的所有连线
        node.getStartNodes().forEach(start -> lines.add(new Line(start, node)));
        node.getEndNodes().forEach(end -> lines.add(new Line(node, end)));

        // 移除节点所在关联的连线
        node.cleanRelations();
        return true;
    }

    @Override
    public void revert() {
        // 重建连线关系
        lines.forEach(Line::relation);
    }
}
