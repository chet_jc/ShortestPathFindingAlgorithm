package tk.winshu.shortestpath.model;

import java.util.List;

/**
 * 用于导入导出
 *
 * @author winshu
 * @since 2019/10/19.
 */
public class NodeData {

    private List<Node> nodes;

    private int startIndex;

    private int endIndex;

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public Node getStartNode() {
        if (nodes == null) {
            return null;
        }
        for (Node n : nodes) {
            if (n.getIndex() == startIndex) {
                return n;
            }
        }
        return null;
    }

    public Node getEndNode() {
        if (nodes == null) {
            return null;
        }
        for (Node n : nodes) {
            if (n.getIndex() == endIndex) {
                return n;
            }
        }
        return null;
    }

    public int getNodeCount() {
        return nodes == null ? 0 : nodes.size();
    }
}
