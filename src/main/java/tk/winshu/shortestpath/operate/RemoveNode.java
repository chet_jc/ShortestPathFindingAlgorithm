package tk.winshu.shortestpath.operate;

import tk.winshu.shortestpath.model.Node;

/**
 * 移除节点=清除节点所在连线+删除节点
 *
 * @author winshu
 * @since 2019/10/28
 */
@OperateType(name = "移除节点")
public class RemoveNode extends AbstractOperate {

    private CleanLines cleanLines;
    private Node node;

    public RemoveNode(Node node) {
        this.cleanLines = new CleanLines(node);
        this.node = node;
    }

    @Override
    public boolean execute() {
        return cleanLines.execute() && nodes.remove(node);
    }

    @Override
    public void revert() {
        cleanLines.revert();
        nodes.add(node);
    }
}
