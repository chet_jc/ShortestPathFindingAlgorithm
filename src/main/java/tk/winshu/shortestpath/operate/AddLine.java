package tk.winshu.shortestpath.operate;

import tk.winshu.shortestpath.model.Node;

/**
 * 添加连线
 *
 * @author winshu
 * @since 2019/10/28
 */
@OperateType(name = "添加连线")
public class AddLine implements IOperate {

    private Line line;

    public AddLine(Node start, Node end) {
        this.line = new Line(start, end);
    }

    @Override
    public boolean execute() {
        return line.relation();
    }

    @Override
    public void revert() {
        line.release();
    }
}
