package drawline;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * 测试绘制连线
 */
public class DrawLineTest extends JFrame {

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                DrawLineTest frame = new DrawLineTest();
                frame.setVisible(true);
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        });
    }

    private DrawLineTest() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        CanvasPanel canvasPanel = new CanvasPanel();
        contentPane.add(canvasPanel, BorderLayout.CENTER);
    }

}
