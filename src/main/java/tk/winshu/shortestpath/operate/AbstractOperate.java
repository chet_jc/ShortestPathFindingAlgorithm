package tk.winshu.shortestpath.operate;

import tk.winshu.shortestpath.model.Node;

import java.util.List;

/**
 * 需要操作 节点集的操作
 * @author winshu
 * @since 2019/10/28
 */
public abstract class AbstractOperate implements IOperate {

    protected List<Node> nodes;

    @Override
    public void init(List<Node> nodes) {
        this.nodes = nodes;
    }

    protected boolean addNode(Node node) {
        if (nodes.contains(node)) {
            return false;
        }
        return nodes.add(node);
    }

    protected boolean removeNode(Node node) {
        return nodes.remove(node);
    }

    protected Node getNode(int index) {
        return nodes.get(index);
    }

}
