package tk.winshu.shortestpath.run;

/**
 * @author winshu
 * @since 2020/11/08.
 */
@FunctionalInterface
public interface IRepaint {

    void repaint();
}
