package tk.winshu.shortestpath.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;

/**
 * 节点状态信息
 *
 * @author winshu
 * @since 2019/10/19.
 */
public enum NodeStatus {

    /**
     * 焦点
     */
    FOCUSED(Color.CYAN, "node_focused.png"),
    /**
     * 起点
     */
    START(Color.MAGENTA, "node_start.png"),
    /**
     * 终点
     */
    END(Color.YELLOW, "node_end.png"),
    /**
     * 默认
     */
    DEFAULT(Color.WHITE, "node_default.png"),

    /**
     * 临时
     */
    TEMP(Color.RED, "node_default.png");

    NodeStatus(Color color, String image) {
        this.color = color;
        this.image = loadImage(image);
    }

    static final Logger log = LoggerFactory.getLogger(NodeStatus.class);

    private Image image;

    private Color color;

    public Image getImage() {
        return image;
    }

    public Color getColor() {
        return color;
    }

    public boolean isDrawShape() {
        return !DEFAULT.equals(this) && !TEMP.equals(this);
    }

    private static Image loadImage(String image) {
        try {
            return ImageIO.read(NodeStatus.class.getResource("/image/" + image));
        } catch (IOException e) {
            log.error("Can't load image: " + image, e);
        }
        return null;
    }
}
