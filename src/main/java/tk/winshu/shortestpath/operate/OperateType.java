package tk.winshu.shortestpath.operate;

import java.lang.annotation.*;

/**
 * @author winshu
 * @since 2019/10/29
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface OperateType {

    String name();
}
