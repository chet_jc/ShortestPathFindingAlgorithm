package tk.winshu.shortestpath.model;

import java.awt.*;

/**
 * @author jason
 * @since 2019/10/19.
 */
public class Runner extends NObject {

    private Image image;

    private boolean isVisible;

    public Runner(Image image) {
        super(0, 0, 0);
        this.image = image;
        this.isVisible = false;
    }

    public void draw(Graphics2D g2d) {
        if (isVisible) {
            super.draw(g2d, image, Color.WHITE, false);
        }
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }
}
